﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace footprints.smartcalltech.co.za.Models.linq.avm
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="avm.stsp")]
	public partial class avmDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertRequest(Request instance);
    partial void UpdateRequest(Request instance);
    partial void DeleteRequest(Request instance);
    partial void InsertRequest_Archice_A(Request_Archice_A instance);
    partial void UpdateRequest_Archice_A(Request_Archice_A instance);
    partial void DeleteRequest_Archice_A(Request_Archice_A instance);
    #endregion
		
		public avmDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["avm_stspConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public avmDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public avmDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public avmDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public avmDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<Request> Requests
		{
			get
			{
				return this.GetTable<Request>();
			}
		}
		
		public System.Data.Linq.Table<Request_Archice_A> Request_Archice_As
		{
			get
			{
				return this.GetTable<Request_Archice_A>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Request")]
	public partial class Request : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _Id;
		
		private System.DateTime _DateTime;
		
		private string _Campaign;
		
		private System.Nullable<int> _CampaignId;
		
		private string _Msisdn;
		
		private string _AVMRequestResponse;
		
		private System.Nullable<System.DateTime> _Answered;
		
		private System.Nullable<System.DateTime> _Hangup;
		
		private System.Nullable<int> _Duration;
		
		private string _Result;
		
		private string _CallEndDate;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdChanging(int value);
    partial void OnIdChanged();
    partial void OnDateTimeChanging(System.DateTime value);
    partial void OnDateTimeChanged();
    partial void OnCampaignChanging(string value);
    partial void OnCampaignChanged();
    partial void OnCampaignIdChanging(System.Nullable<int> value);
    partial void OnCampaignIdChanged();
    partial void OnMsisdnChanging(string value);
    partial void OnMsisdnChanged();
    partial void OnAVMRequestResponseChanging(string value);
    partial void OnAVMRequestResponseChanged();
    partial void OnAnsweredChanging(System.Nullable<System.DateTime> value);
    partial void OnAnsweredChanged();
    partial void OnHangupChanging(System.Nullable<System.DateTime> value);
    partial void OnHangupChanged();
    partial void OnDurationChanging(System.Nullable<int> value);
    partial void OnDurationChanged();
    partial void OnResultChanging(string value);
    partial void OnResultChanged();
    partial void OnCallEndDateChanging(string value);
    partial void OnCallEndDateChanged();
    #endregion
		
		public Request()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DateTime", DbType="DateTime NOT NULL")]
		public System.DateTime DateTime
		{
			get
			{
				return this._DateTime;
			}
			set
			{
				if ((this._DateTime != value))
				{
					this.OnDateTimeChanging(value);
					this.SendPropertyChanging();
					this._DateTime = value;
					this.SendPropertyChanged("DateTime");
					this.OnDateTimeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Campaign", DbType="NVarChar(55) NOT NULL", CanBeNull=false)]
		public string Campaign
		{
			get
			{
				return this._Campaign;
			}
			set
			{
				if ((this._Campaign != value))
				{
					this.OnCampaignChanging(value);
					this.SendPropertyChanging();
					this._Campaign = value;
					this.SendPropertyChanged("Campaign");
					this.OnCampaignChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CampaignId", DbType="Int")]
		public System.Nullable<int> CampaignId
		{
			get
			{
				return this._CampaignId;
			}
			set
			{
				if ((this._CampaignId != value))
				{
					this.OnCampaignIdChanging(value);
					this.SendPropertyChanging();
					this._CampaignId = value;
					this.SendPropertyChanged("CampaignId");
					this.OnCampaignIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Msisdn", DbType="NVarChar(20) NOT NULL", CanBeNull=false)]
		public string Msisdn
		{
			get
			{
				return this._Msisdn;
			}
			set
			{
				if ((this._Msisdn != value))
				{
					this.OnMsisdnChanging(value);
					this.SendPropertyChanging();
					this._Msisdn = value;
					this.SendPropertyChanged("Msisdn");
					this.OnMsisdnChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_AVMRequestResponse", DbType="NVarChar(20)")]
		public string AVMRequestResponse
		{
			get
			{
				return this._AVMRequestResponse;
			}
			set
			{
				if ((this._AVMRequestResponse != value))
				{
					this.OnAVMRequestResponseChanging(value);
					this.SendPropertyChanging();
					this._AVMRequestResponse = value;
					this.SendPropertyChanged("AVMRequestResponse");
					this.OnAVMRequestResponseChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Answered", DbType="DateTime")]
		public System.Nullable<System.DateTime> Answered
		{
			get
			{
				return this._Answered;
			}
			set
			{
				if ((this._Answered != value))
				{
					this.OnAnsweredChanging(value);
					this.SendPropertyChanging();
					this._Answered = value;
					this.SendPropertyChanged("Answered");
					this.OnAnsweredChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Hangup", DbType="DateTime")]
		public System.Nullable<System.DateTime> Hangup
		{
			get
			{
				return this._Hangup;
			}
			set
			{
				if ((this._Hangup != value))
				{
					this.OnHangupChanging(value);
					this.SendPropertyChanging();
					this._Hangup = value;
					this.SendPropertyChanged("Hangup");
					this.OnHangupChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Duration", DbType="Int")]
		public System.Nullable<int> Duration
		{
			get
			{
				return this._Duration;
			}
			set
			{
				if ((this._Duration != value))
				{
					this.OnDurationChanging(value);
					this.SendPropertyChanging();
					this._Duration = value;
					this.SendPropertyChanged("Duration");
					this.OnDurationChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Result", DbType="NVarChar(20)")]
		public string Result
		{
			get
			{
				return this._Result;
			}
			set
			{
				if ((this._Result != value))
				{
					this.OnResultChanging(value);
					this.SendPropertyChanging();
					this._Result = value;
					this.SendPropertyChanged("Result");
					this.OnResultChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CallEndDate", DbType="NVarChar(20)")]
		public string CallEndDate
		{
			get
			{
				return this._CallEndDate;
			}
			set
			{
				if ((this._CallEndDate != value))
				{
					this.OnCallEndDateChanging(value);
					this.SendPropertyChanging();
					this._CallEndDate = value;
					this.SendPropertyChanged("CallEndDate");
					this.OnCallEndDateChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Request_Archice_A")]
	public partial class Request_Archice_A : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _Id;
		
		private System.DateTime _DateTime;
		
		private string _Campaign;
		
		private System.Nullable<int> _CampaignId;
		
		private string _Msisdn;
		
		private string _AVMRequestResponse;
		
		private System.Nullable<System.DateTime> _Answered;
		
		private System.Nullable<System.DateTime> _Hangup;
		
		private System.Nullable<int> _Duration;
		
		private string _Result;
		
		private string _CallEndDate;
		
		private string _Tag;
		
		private string _IP;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdChanging(int value);
    partial void OnIdChanged();
    partial void OnDateTimeChanging(System.DateTime value);
    partial void OnDateTimeChanged();
    partial void OnCampaignChanging(string value);
    partial void OnCampaignChanged();
    partial void OnCampaignIdChanging(System.Nullable<int> value);
    partial void OnCampaignIdChanged();
    partial void OnMsisdnChanging(string value);
    partial void OnMsisdnChanged();
    partial void OnAVMRequestResponseChanging(string value);
    partial void OnAVMRequestResponseChanged();
    partial void OnAnsweredChanging(System.Nullable<System.DateTime> value);
    partial void OnAnsweredChanged();
    partial void OnHangupChanging(System.Nullable<System.DateTime> value);
    partial void OnHangupChanged();
    partial void OnDurationChanging(System.Nullable<int> value);
    partial void OnDurationChanged();
    partial void OnResultChanging(string value);
    partial void OnResultChanged();
    partial void OnCallEndDateChanging(string value);
    partial void OnCallEndDateChanged();
    partial void OnTagChanging(string value);
    partial void OnTagChanged();
    partial void OnIPChanging(string value);
    partial void OnIPChanged();
    #endregion
		
		public Request_Archice_A()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DateTime", DbType="DateTime NOT NULL")]
		public System.DateTime DateTime
		{
			get
			{
				return this._DateTime;
			}
			set
			{
				if ((this._DateTime != value))
				{
					this.OnDateTimeChanging(value);
					this.SendPropertyChanging();
					this._DateTime = value;
					this.SendPropertyChanged("DateTime");
					this.OnDateTimeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Campaign", DbType="NVarChar(55) NOT NULL", CanBeNull=false)]
		public string Campaign
		{
			get
			{
				return this._Campaign;
			}
			set
			{
				if ((this._Campaign != value))
				{
					this.OnCampaignChanging(value);
					this.SendPropertyChanging();
					this._Campaign = value;
					this.SendPropertyChanged("Campaign");
					this.OnCampaignChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CampaignId", DbType="Int")]
		public System.Nullable<int> CampaignId
		{
			get
			{
				return this._CampaignId;
			}
			set
			{
				if ((this._CampaignId != value))
				{
					this.OnCampaignIdChanging(value);
					this.SendPropertyChanging();
					this._CampaignId = value;
					this.SendPropertyChanged("CampaignId");
					this.OnCampaignIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Msisdn", DbType="NVarChar(20) NOT NULL", CanBeNull=false)]
		public string Msisdn
		{
			get
			{
				return this._Msisdn;
			}
			set
			{
				if ((this._Msisdn != value))
				{
					this.OnMsisdnChanging(value);
					this.SendPropertyChanging();
					this._Msisdn = value;
					this.SendPropertyChanged("Msisdn");
					this.OnMsisdnChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_AVMRequestResponse", DbType="NVarChar(20)")]
		public string AVMRequestResponse
		{
			get
			{
				return this._AVMRequestResponse;
			}
			set
			{
				if ((this._AVMRequestResponse != value))
				{
					this.OnAVMRequestResponseChanging(value);
					this.SendPropertyChanging();
					this._AVMRequestResponse = value;
					this.SendPropertyChanged("AVMRequestResponse");
					this.OnAVMRequestResponseChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Answered", DbType="DateTime")]
		public System.Nullable<System.DateTime> Answered
		{
			get
			{
				return this._Answered;
			}
			set
			{
				if ((this._Answered != value))
				{
					this.OnAnsweredChanging(value);
					this.SendPropertyChanging();
					this._Answered = value;
					this.SendPropertyChanged("Answered");
					this.OnAnsweredChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Hangup", DbType="DateTime")]
		public System.Nullable<System.DateTime> Hangup
		{
			get
			{
				return this._Hangup;
			}
			set
			{
				if ((this._Hangup != value))
				{
					this.OnHangupChanging(value);
					this.SendPropertyChanging();
					this._Hangup = value;
					this.SendPropertyChanged("Hangup");
					this.OnHangupChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Duration", DbType="Int")]
		public System.Nullable<int> Duration
		{
			get
			{
				return this._Duration;
			}
			set
			{
				if ((this._Duration != value))
				{
					this.OnDurationChanging(value);
					this.SendPropertyChanging();
					this._Duration = value;
					this.SendPropertyChanged("Duration");
					this.OnDurationChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Result", DbType="NVarChar(20)")]
		public string Result
		{
			get
			{
				return this._Result;
			}
			set
			{
				if ((this._Result != value))
				{
					this.OnResultChanging(value);
					this.SendPropertyChanging();
					this._Result = value;
					this.SendPropertyChanged("Result");
					this.OnResultChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CallEndDate", DbType="NVarChar(20)")]
		public string CallEndDate
		{
			get
			{
				return this._CallEndDate;
			}
			set
			{
				if ((this._CallEndDate != value))
				{
					this.OnCallEndDateChanging(value);
					this.SendPropertyChanging();
					this._CallEndDate = value;
					this.SendPropertyChanged("CallEndDate");
					this.OnCallEndDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Tag", DbType="NVarChar(MAX)")]
		public string Tag
		{
			get
			{
				return this._Tag;
			}
			set
			{
				if ((this._Tag != value))
				{
					this.OnTagChanging(value);
					this.SendPropertyChanging();
					this._Tag = value;
					this.SendPropertyChanged("Tag");
					this.OnTagChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IP", DbType="NVarChar(MAX)")]
		public string IP
		{
			get
			{
				return this._IP;
			}
			set
			{
				if ((this._IP != value))
				{
					this.OnIPChanging(value);
					this.SendPropertyChanging();
					this._IP = value;
					this.SendPropertyChanged("IP");
					this.OnIPChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
