﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace footprints.smartcalltech.co.za.Models
{
    public class user
    {
        public bool filterbydefault { get; set; }
        public bool bulklookup { get; set; }
        public string domain { get; set; }
        public int id { get; set; }
        public string username  { get; set; }

        public string ranges { get; set; }

        public string defaultrange { get; set; }
    }
}