﻿using System.Web;
using System.Web.Mvc;

namespace footprints.smartcalltech.co.za
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
