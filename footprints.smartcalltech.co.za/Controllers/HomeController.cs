﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;

namespace footprints.smartcalltech.co.za.Controllers
{

    public class Log
    {
        public enum EntryAction
        {
            Search ,
            Dnb , 
            Dnc , 
            Cancel ,
            Login , 
            Download 
        }

        public static void logEntry(int userid, string msisdn, EntryAction action)
        {
            try
            {
                using (footprints.smartcalltech.co.za.Models.linq.fpDataContext dc = new Models.linq.fpDataContext())
                {
                    footprints.smartcalltech.co.za.Models.linq.usertrace tr = new Models.linq.usertrace();
                    tr.Action = action.ToString();
                    tr.DateTime = DateTime.Now;
                    tr.Msisdn = msisdn;
                    tr.UserId = userid;

                    dc.usertraces.InsertOnSubmit(tr);
                    dc.SubmitChanges();
                }
            }
            catch (Exception err)
            {
                new52.Core.Logging.WriteTrace(err);
                throw;
            }
        }
    }

    public class HomeController : Controller
    {
        public ActionResult bulk(string msisdnlist,out List<string> msisdnList )
        {
            msisdnList = new List<string>();
            List<SearchResultEntry> entryList = new List<SearchResultEntry> () ;
            bool hasSubs = false;

            Models.user u = (Models.user)Session["username"];

            if (u == null) { Response.Redirect("/?logout"); };

            string[] str = msisdnlist.Split(new[] { ',', ' ', '\n' },
                                  StringSplitOptions.RemoveEmptyEntries); 

            foreach (string msisdn in str )
            {
                var newmsisdn = GetNumbers(msisdn);
                if (newmsisdn.StartsWith("0")) { newmsisdn = u.defaultrange + msisdn.Substring(1); }
                if (!newmsisdn.StartsWith("2")) { newmsisdn = u.defaultrange + msisdn.Substring(0); }

             
                if (u.filterbydefault && msisdn.StartsWith(u.defaultrange))
                {
                    msisdnList.Add(msisdn);

                    mesh(newmsisdn, ref entryList, ref hasSubs);
                    sdp(newmsisdn, ref entryList, ref hasSubs);
                    ctb(newmsisdn, ref entryList, ref hasSubs);
                }

              
            }

            var csv = ToCsv(entryList);
            var stream = new
                  System.IO.MemoryStream(System.Text.ASCIIEncoding.ASCII.GetBytes(csv));
             
            return new FileStreamResult(stream, "application/text")
            {
                FileDownloadName = Guid.NewGuid().ToString() + "_" + "bulk" + ".csv"
            };
        }

        public ActionResult download(string msisdn)
        {

            try
            {
                // var filestream = System.IO.File.ReadAllBytes(@"path/sourcefilename.pdf");

                Models.user u = (Models.user)Session["username"];
                if (u == null) { Response.Redirect("/?logout"); };

                Log.logEntry(u.id, msisdn, Log.EntryAction.Download);

                SearchResult sr = (SearchResult)Session[msisdn];
                var csv = ToCsv(sr.entries);
                
                var stream = new
                    System.IO.MemoryStream(System.Text.ASCIIEncoding.ASCII.GetBytes(csv));


                return new FileStreamResult(stream, "application/text")
                {
                    FileDownloadName = Guid.NewGuid().ToString() + "_" + sr.msisdn + ".csv"
                };
            }
            catch (Exception err)
            {
                return Json(err.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public void getTodayTitle()
        {
            return;
            string todayTitle = "";

            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                using (System.Net.WebClient wc = new System.Net.WebClient())
                {
                    var content = wc.DownloadString("https://www.daysoftheyear.com");
                    string startIndexString = "id=\"homepage-banner-click-target\">";
                    int startIndex = content.IndexOf(startIndexString);
                    content = content.Substring(startIndex);
                    string stopIndexString = ("</a>");
                    int stopIndex = content.IndexOf(stopIndexString);
                    content = content.Substring(0, stopIndex);
                    content = content.Substring(34);

                    todayTitle = "today is '" + content + "'";
                }
            }
            catch (Exception err)
            {
                //new52.Core.Logging.WriteTrace(err);
            }
            ViewBag.Today = todayTitle;
        }

        bool isDNC(string msisdn)
        {
            try
            {

                // SearchResult sr = (SearchResult)Session[msisdn];

                using (Models.linq.esme.esmeDataContext dc = new Models.linq.esme.esmeDataContext())
                {
                    var query = from zz in dc.DNCs
                                where zz.Msisdn == msisdn
                                select zz;

                    if (query.Count() == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception err)
            {
                new52.Core.Logging.WriteTrace(err);
                return false;
            }
        }

        public ActionResult dnc(string msisdn)
        {
            try
            {

                Models.user u = (Models.user)Session["username"];

                if (u == null) { Response.Redirect("/?logout"); };

                Log.logEntry(u.id, msisdn, Log.EntryAction.Dnc);

                SearchResult sr = (SearchResult)Session[msisdn];

                using (Models.linq.esme.esmeDataContext dc = new Models.linq.esme.esmeDataContext())
                {
                    Models.linq.esme.DNC dnc = new Models.linq.esme.DNC();
                    dnc.DateTime = DateTime.Now;
                    dnc.Msisdn = sr.msisdn;

                    dc.DNCs.InsertOnSubmit(dnc);
                    dc.SubmitChanges();
                }

                return Json("true", JsonRequestBehavior.AllowGet);
            }
            catch (Exception err)
            {
                new52.Core.Logging.WriteTrace(err);
                return Json(err.Message, JsonRequestBehavior.AllowGet);
            }
        }



        string isPorted (string msisdn)
        {

          //  var portedResult = ""; 
            try
            {

                // SearchResult sr = (SearchResult)Session[msisdn];

                using (Models.linq.nmp.nmpDataContext dc = new Models.linq.nmp.nmpDataContext())
                {
                    var query = from zz in dc.Routes
                                where zz.Msisdn == msisdn
                                orderby zz.Id descending
                                select zz;

                    if (query.Count() > 0)
                    {
                        return query.ToArray()[0].MsisdnRoute;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch (Exception err)
            {
                new52.Core.Logging.WriteTrace(err);
                return err.Message;
            }
        }

        bool isDNB(string msisdn)
        {
            try
            {

                // SearchResult sr = (SearchResult)Session[msisdn];

                using (Models.linq.obs.obsDataContext dc = new Models.linq.obs.obsDataContext())
                {
                    var query = from zz in dc.Blockeds
                                where zz.Msisdn == msisdn
                                select zz;

                    if (query.Count() == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception err)
            {
                new52.Core.Logging.WriteTrace(err);
                return false;
            }
        }



        public ActionResult dnb(string msisdn)
        {
            try
            {


                Models.user u = (Models.user)Session["username"];

                if (u == null) { Response.Redirect("/?logout"); };


                Log.logEntry(u.id, msisdn, Log.EntryAction.Dnb);



                SearchResult sr = (SearchResult)Session[msisdn];

                using (Models.linq.obs.obsDataContext dc = new Models.linq.obs.obsDataContext())
                {
                    Models.linq.obs.Blocked dnc = new Models.linq.obs.Blocked();
                    dnc.Date = DateTime.Now;
                    dnc.Msisdn = sr.msisdn;
                    dnc.Reason = "footprints.smartcalltech.co.za " + u.username;

                    dc.Blockeds.InsertOnSubmit(dnc);
                    dc.SubmitChanges();
                }

                return Json("true", JsonRequestBehavior.AllowGet);
            }
            catch (Exception err)
            {
                new52.Core.Logging.WriteTrace(err);
                return Json(err.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult cancel(string msisdn)
        {

            try
            {
                Models.user u = (Models.user)Session["username"];

                if (u == null) { Response.Redirect("/?logout"); };

                Log.logEntry(u.id, msisdn, Log.EntryAction.Cancel);

                //SearchResult sr = (SearchResult)Session[msisdn];
                new52.Core.Notification
                    .SendMail("application1", "Mumtaz123", "application1@smartcalltech.co.za", "pabx@smartcalltech.co.za;"
                    , msisdn, "", new List<string>(), true);

                cancelonsdp(msisdn);

                return Json("true", JsonRequestBehavior.AllowGet);
            }
            catch (Exception err)
            {
                new52.Core.Logging.WriteTrace(err);
                return Json(err.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public void cancelonsdp(string msisdn)
        {
            try
            {


                using (Models.linq.sdp.sdpDataContext dc = new Models.linq.sdp.sdpDataContext())
                {
                    var query = from zz in dc.Subscribers
                                where zz.Msisdn == msisdn
                                select zz;

                    dc.Subscribers.DeleteAllOnSubmit(query);
                    dc.SubmitChanges();
                }
            }
            catch (Exception err)
            {
                new52.Core.Logging.WriteTrace(err);
                throw err;
            }
        }

   

        public ActionResult Index()
        {
            ViewBag.Error = null;
             
            SearchResult sr = new SearchResult();
            Models.user u = null;

            try
            {

                ViewBag.hasSubscription = false;

                #region init
                if (Request.QueryString.Count == 0)
                {

                    return View(new List<SearchResultEntry>());
                }


                string msisdn = null;
                if (Request.QueryString["msisdn"] != null && Request.QueryString["msisdn"] != "")
                {
                    u = (Models.user)Session["username"];
                    if (u == null) { Response.Redirect("/?logout"); }; 

                    msisdn = Request.QueryString["msisdn"].ToString();

                    List<string> msisdnList = null ;
                    if (u.bulklookup && (msisdn.Contains(",") || msisdn.Contains("\n"))) { return bulk(msisdn, out msisdnList); sr.bulkList = msisdnList; sr.isBulk = true; }

                    msisdn = GetNumbers(msisdn);
                     
                    if (msisdn.StartsWith("0")) { msisdn = u.defaultrange + msisdn.Substring(1); }
                    if (!msisdn.StartsWith("2")) { msisdn = u.defaultrange + msisdn.Substring(0); }
                    if (msisdn.StartsWith("2609595")) { msisdn = msisdn.Replace("2609595", "26095"); }

                }

                if (msisdn.Length == 0 || msisdn.Length < 11)
                {
                    return View(new List<SearchResultEntry>());
                }
                #endregion

                if (Request.QueryString.Count > 1)
                {

                    sr = (SearchResult)Session[msisdn];
                    ViewBag.sr = sr;
                    return View(sr.entries);
                }
                else
                {
                    Log.logEntry(u.id, msisdn, Log.EntryAction.Search);

                    sr = new SearchResult();
                    sr.msisdn = msisdn;
                     
                    STS.Core.Msisdn msisdnO = new STS.Core.Msisdn(msisdn);
                    sr.network = msisdnO.Network.ToString().Replace("Network_", "");
                    sr.dnc = isDNC(msisdn);
                    sr.dnb = isDNB(msisdn);
                    sr.ported = isPorted(msisdn);


                    List<SearchResultEntry> sList = new List<SearchResultEntry>();


                    bool hasSubscription = false;

                    if (u.domain == "MTN")
                    {
                        if ((msisdnO.Network != STS.Core.Enums.Networks.Network_MTN)

                           && (msisdn != "27824052382" && msisdn != "27649560009" && msisdn != "27824051595") 
                            
                            )
                        {
                            throw new Exception("Invalid Network cellphone number provided");
                        }

                        avm(msisdn, ref sList);
                       // ctb(msisdn, ref sList, ref hasSubscription);

                        
                    }


                    else if (u.domain == "Vodacom")
                    {
                        if (msisdnO.Network != STS.Core.Enums.Networks.Network_Vodacom)
                        {
                            throw new Exception ("Invalid Vodacom cellphone number provided") ;
                        }

                        avm(msisdn, ref sList);
                        ctb(msisdn, ref sList, ref hasSubscription);
                      
                      
                    }
                    else if (u.domain == "Zamtel")
                    {
                          if (msisdnO.Network != STS.Core.Enums.Networks.Network_Zambia_ZAMTEL)
                        {
                            throw new Exception("Invalid Vodacom cellphone number provided");
                        }

                        sdp(msisdn, ref sList,ref hasSubscription);
                        obs(msisdn, ref sList);
                        SMS(msisdn, ref sList);
                    }
                    else
                    {
                        avm(msisdn, ref sList);

                        if (msisdnO.Network == STS.Core.Enums.Networks.Network_Vodacom)
                        {
                            ctb(msisdn, ref sList, ref hasSubscription);
                        }
                       
                        sdp(msisdn, ref sList , ref hasSubscription);

                        SMS(msisdn, ref sList);
                        obs(msisdn, ref sList);
                        mesh(msisdn, ref sList, ref hasSubscription);
                    }
            

                    var querySorted = from zz in sList
                                      orderby zz.DateTime descending
                                      select zz;
                    sr.entries = querySorted.ToList();

                    sr.hassubscription = hasSubscription;
                    ViewBag.sr = sr;
                    Session[msisdn] = sr; // for page callback
                    return View(sr.entries);
                }


            }
            catch (Exception err)
            {
                if (err.Message != "Object reference not set to an instance of an object.")
                {
                    ViewBag.Error = err.Message;
                    ViewBag.sr = sr;
                }
                return View(new List<SearchResultEntry>());
            }
        }

        private static string GetNumbers(string input)
        {
            return new string(input.Where(c => char.IsDigit(c)).ToArray());
        }


        private void avm(string msisdn, ref List<SearchResultEntry> sList)
        {
            using (Models.linq.avm.avmDataContext dc = new Models.linq.avm.avmDataContext())
            {
                var query = (from zz in dc.Requests
                             where zz.Msisdn == msisdn
                             orderby zz.DateTime descending
                             select zz).Take(Int32.MaxValue);

                foreach (Models.linq.avm.Request ss in query)
                {
                    SearchResultEntry s = new SearchResultEntry();
                    s.Service = ss.Campaign;
                    s.DateTime = ss.DateTime.ToString("yy-MM-dd HH:mm");
                    s.Type = "Avm";
                    s.EventData = ss.Result + " Call Duration :" + ss.Duration.Value  ;
                    s.Msisdn = ss.Msisdn;

                    sList.Add(s);
                }
            }

            //using (Models.linq.avm.avmDataContext dc = new Models.linq.avm.avmDataContext())
            //{
            //    var query = (from zz in dc.Request_Archice_As
            //                 where zz.Msisdn == msisdn
            //                 orderby zz.DateTime descending
            //                 select zz).Take(Int32.MaxValue);

            //    foreach (Models.linq.avm.Request_Archice_A ss in query)
            //    {
            //        SearchResultEntry s = new SearchResultEntry();
            //        s.Service = ss.Campaign;
            //        s.DateTime = ss.DateTime.ToString("yy-MM-dd HH:mm");
            //        s.Type = "Avm";
            //        s.EventData = ss.Result + " Call Duration :" + ss.Duration.Value  ;
            //        s.Msisdn = ss.Msisdn;

            //        sList.Add(s);
            //    }
            //}
        }

        private void sdp(string msisdn, ref List<SearchResultEntry> sList , ref bool hassubscription)
        {
            using (Models.linq.sdp.sdpDataContext dc = new Models.linq.sdp.sdpDataContext())
            {
                var query = (from zz in dc.Subscribers
                             where zz.Msisdn == msisdn
                             orderby zz.CreationDate descending
                             select zz).Take(Int32.MaxValue);

                foreach (Models.linq.sdp.Subscriber ss in query)
                {

                    hassubscription = true;

                    SearchResultEntry s = new SearchResultEntry();
                    s.Service = get_servicename(ss.OBSGuid);
                    s.DateTime = ss.CreationDate.ToString("yy-MM-dd HH:mm");
                    s.Type = "Sdp";
                    s.EventData = "SUBSCRIBED " + ss.Ref + " " + ss.PubId;
                    s.Msisdn = ss.Msisdn;

                    sList.Add(s);
                }
            }
        }

        public void obs(string msisdn, ref List<SearchResultEntry> sList)
        {
            try
            {
                using (System.Data.SqlClient.SqlDataReader reader =
                    Toolbox.Data.SqlHelper.ExecuteReader("server=10.1.1.61;database =sp ; user id = sa ; Password = Mumtaz123", "obs_data_msisdn", msisdn))
                {
                    while (reader.Read())
                    {
                        SearchResultEntry s = new SearchResultEntry();
                        s.Service = reader["Service"].ToString();
                        s.DateTime = Convert.ToDateTime(reader["date"]).ToString("yy-MM-dd HH:mm");
                        s.Type = "Billing";
                        s.EventData = reader["status"].ToString() + "\t" + Convert.ToDecimal(reader["amount"]).ToString("0.00");
                        if (Convert.ToBoolean ( reader["transactionsuccess"] )  )
                        {
                            s.Cost = Convert.ToDecimal(reader["amount"]).ToString("0.00"); 
                        }
                        sList.Add(s);
                    }


                }

            }
            catch (Exception err)
            {
                new52.Core.Logging.WriteTrace(err);
            }
        }

        public void SMS(string msisdn, ref List<SearchResultEntry> sList)
        {
            try
            {
                using (System.Data.SqlClient.SqlDataReader reader = Toolbox.Data.SqlHelper.ExecuteReader("server=10.3.1.50;database =esme ; user id = sa ; Password = Mumtaz123", "usp_MOMTLookup", msisdn))
                {
                    while (reader.Read())
                    {
                        SearchResultEntry s = new SearchResultEntry();
                        s.Service = reader["Campaign"].ToString();
                        s.DateTime = Convert.ToDateTime(reader["date"]).ToString("yy-MM-dd HH:mm");
                        s.Type = "Mo";
                        s.EventData = "RECEIVED " + reader["DestinationAddress"].ToString() + "\t" + reader["message"].ToString();
                        sList.Add(s);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        SearchResultEntry s = new SearchResultEntry();
                        s.Service = reader["Campaign"].ToString();
                        s.DateTime = Convert.ToDateTime(reader["date"]).ToString("yy-MM-dd HH:mm");
                        s.Type = "Mt";
                        s.EventData = reader["DeliveredMessage"].ToString() + "\t" + reader["message"].ToString();
                        sList.Add(s);
                    }
                }

            }
            catch (Exception err)
            {
                new52.Core.Logging.WriteTrace( err);
            }
        }

        public  string ToCsv( IEnumerable<SearchResultEntry> source)
        {
            if (source == null) throw new ArgumentNullException("source");
            var result = string.Join(",", source.Select(s => s.ToString()).ToArray());
            result = result.Substring(1);
            return result;
        }

        public void mesh(string msisdn, ref  List<SearchResultEntry> sList, ref bool hasSubscription)
        {


            using (Models.linq.sbas.sbasDataContext dc = new Models.linq.sbas.sbasDataContext())
            {
                var query = (from zz in dc.meshrequests
                             where zz.Msisdn == msisdn
                             orderby zz.DateTime descending
                             select zz).Take(Int32.MaxValue);

                foreach (Models.linq.sbas.meshrequest ss in query)
                {

                    try
                    {
                        hasSubscription = true;

                        SearchResultEntry s = new SearchResultEntry();
                        s.Service = ss.ServiceName;
                        s.DateTime = ss.DateTime.ToString("yy-MM-dd HH:mm");
                        s.Type = "Mesh";
                        if (ss.Result != null)
                        {
                            s.EventData = ss.Result.ToUpper();
                            if (ss.Result.ToUpper().StartsWith("CONFIRM"))
                            {
                                hasSubscription = true;
                            }
                        }
                        s.Msisdn = ss.Msisdn;

                        sList.Add(s);
                    }
                    catch (Exception err)
                    {
                        new52.Core.Logging.WriteTrace(err);
                    }
                }
            }
        }

        public void ctb(string msisdn, ref  List<SearchResultEntry> sList, ref bool hasSubscription)
        {

            try
            {
                using (Models.linq.ctb.ctbDataContext dc = new Models.linq.ctb.ctbDataContext())
                {
                    var query = (from zz in dc.PurchaseRequests
                                 where zz.Msisdn == msisdn
                                 orderby zz.Datetime descending
                                 select zz).Take(Int32.MaxValue);

                    foreach (Models.linq.ctb.PurchaseRequest ss in query)
                    {
                        hasSubscription = true;

                        SearchResultEntry s = new SearchResultEntry();
                        s.Service = ss.SID;
                        s.DateTime = ss.Datetime.ToString("yy-MM-dd HH:mm");
                        s.Type = "Ctb";

                        if (ss.Success.HasValue && ss.Success == true)
                        {
                            s.EventData = "CONFIRM (" + ss.Status + ")\r" + ss.SMSText;
                        }
                        else
                        {
                            s.EventData = ss.Exception;
                        }

                        if (ss.Type == "Cancel Request")
                        {
                            s.Type = "Ctb Cancel";
                            s.EventData = "";

                        }

                        s.Msisdn = ss.Msisdn;

                        sList.Add(s);
                    }

                }
            }
            catch (Exception err)
            {
                new52.Core.Logging.WriteTrace(err);
            }
        }

        public class SearchResultEntry
        {
            public string DateTime { get; set; }

            public string Type { get; set; }
            public string Service { get; set; }

            public string Msisdn { get; set; }
            public string EventData { get; set; }

            public string Cost { get; set; }

            public override string ToString()
            {
                return this.DateTime + "," + this.Type + "," + this.Service + "," + this.Msisdn + "," + this.EventData + "\n";
                //return base.ToString();
            }

        }

        public class SearchResult
        {

            public List<string> bulkList = new List<string>();

            public bool isBulk = false;

            public string network;

            public string ported;
            public string msisdn { get; set; }
            public bool hassubscription { get; set; }

            public bool dnb { get; set; }

            public bool dnc { get; set; }

            public List<SearchResultEntry> entries { get; set; }
        }

        Dictionary<string, string> cache_servinenames = new Dictionary<string, string>();
        public string get_servicename(string guid)
        {

            if (!cache_servinenames.ContainsKey(guid))
            {
                using (Models.linq.sdp.sdpDataContext dc = new Models.linq.sdp.sdpDataContext())
                {
                    var query = (from zz in dc.Services
                                 where zz.OBSGuid == guid
                                 select zz).First();

                    cache_servinenames.Add(guid, query.Name);

                }
            }
            return cache_servinenames[guid];
        }

    }

}