﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace footprints.smartcalltech.co.za.Controllers
{
    public class AuthController : Controller
    {

        public ActionResult hasSession ()
        {
              if (Session["username"] == null )
              { return Json("false", JsonRequestBehavior.AllowGet); }
              return Json("true", JsonRequestBehavior.AllowGet);
        }

        public ActionResult logout()
        {
            Session.Contents.Clear();
            return new RedirectResult("/?logout");
        }

        // GET: /Auth/
        public ActionResult validate(string username , string password)
        {
            try
            {
                using (footprints.smartcalltech.co.za.Models.linq.fpDataContext dc = new Models.linq.fpDataContext())
                {
                    var query = from zz in dc.Users
                                where zz.Email == username && zz.Password == password
                                select zz;

                    if (query.Count() == 1)
                    {
                        Models.user u = new Models.user ();
                        u.id = query.ToArray()[0].Id;
                        u.username = query.ToArray()[0].Email;
                        u.defaultrange = query.ToArray()[0].DefaultRange;
                        u.domain = query.ToArray()[0].Domain;
                        u.bulklookup = query.ToArray()[0].BulkLookup;
                        u.filterbydefault = query.ToArray()[0].BulkLookup; 

                        Controllers.Log.logEntry(u.id, null, Log.EntryAction.Login);
                        
                        Session["username"] = u;
                        return Json("true", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Username and/or password are incorrect.", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception err)
            {
                return Json(err.Message, JsonRequestBehavior.AllowGet);
            }

   
        }
	}
}